package th.in.vector.elrestaurante;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class FoodDetailActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private Intent intent;
    private TextView txtQty;
    private ImageView imageFood;
    private TextView txtFoodName;
    private TextView txtFoodDesc;
    private TextView txtFoodPrice;
    private ImageView btnAdd;
    private ImageView btnDel;
    private TextView btnOrder;

    private String foodName;
    private String foodDesc;
    private String imageUri;
    private double price;
    private int qty;
    private int foodId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_detail);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtQty = (TextView) findViewById(R.id.txtQty);
        txtQty.setKeyListener(null);
        qty = 1;

        imageFood = (ImageView) findViewById(R.id.imageFood);
        btnAdd = (ImageView) findViewById(R.id.btnAdd);
        btnDel = (ImageView) findViewById(R.id.btnDel);
        txtFoodName = (TextView) findViewById(R.id.lblFoodName);
        txtFoodDesc = (TextView) findViewById(R.id.lblFoodDetail);
        txtFoodPrice = (TextView) findViewById(R.id.lblFoodQty);
        btnOrder = (TextView) findViewById(R.id.btnSendChef);

        intent = getIntent();
        Bundle bundle = intent.getExtras();

        if(bundle!=null) {
            foodId   = bundle.getInt(MenuActivity.EXTRA_FOOD_ID);
            foodName = bundle.getString(MenuActivity.EXTRA_FOOD_NAME);
            foodDesc = bundle.getString(MenuActivity.EXTRA_DESC);
            imageUri = bundle.getString(MenuActivity.EXTRA_PICTURE);
            price =    bundle.getDouble(MenuActivity.EXTRA_PRICE);
            Log.d("FoodDetail", "Loaded " + foodName + " " + foodDesc + " " + imageUri + " " + String.valueOf(price));
        }

        txtFoodName.setText(foodName);
        txtFoodDesc.setText(foodDesc);
        txtFoodPrice.setText("Price: "+String.valueOf(price)+" USD");
        Picasso.with(this).load(imageUri).fit().into(imageFood);

        btnAdd.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                qty++;
                if(qty>10) qty = 10;
                txtQty.setText(String.valueOf(qty));
                return false;
            }
        });
        btnDel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                qty--;
                if(qty<1) qty = 1;
                txtQty.setText(String.valueOf(qty));
                return false;
            }
        });

        btnOrder.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra(MenuActivity.EXTRA_FOOD_ID, foodId);
                returnIntent.putExtra(MenuActivity.EXTRA_RETURN_QTY, qty);
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED,returnIntent);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
