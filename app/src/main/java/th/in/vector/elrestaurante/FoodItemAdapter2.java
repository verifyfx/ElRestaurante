package th.in.vector.elrestaurante;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import static android.R.attr.onClick;
import static android.R.id.list;

/**
 * Created by VerifyFX on 30/11/2016.
 */

public class FoodItemAdapter2 extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private List<FoodOrder> mDataSource;

    public FoodItemAdapter2(Context context, List<FoodOrder> items) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public  int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int pos) {
        return mDataSource.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(final int pos, View convertView, final ViewGroup parent) {
        View rowView = mInflater.inflate(R.layout.menu_item, parent, false);
        ImageView thumbnail   = (ImageView) rowView.findViewById(R.id.thumbnail);
        TextView lblFoodTitle = (TextView) rowView.findViewById(R.id.lblFoodTitle);
        TextView lblFoodQty = (TextView) rowView.findViewById(R.id.lblFoodQty);

        FoodOrder foodOrder = (FoodOrder) getItem(pos);
        FoodItem food = foodOrder.getFood();
        lblFoodTitle.setText(food.getFoodName());
        lblFoodQty.setText("Quantity: " + foodOrder.getQty());
        Picasso.with(mContext).load(food.getPictureUrl()).fit().into(thumbnail);

        ImageView delBtn = (ImageView) rowView.findViewById(R.id.delBtn);
        delBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, pos, 0);
            }
        });
        return rowView;
    }
}