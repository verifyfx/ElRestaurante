package th.in.vector.elrestaurante;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by VerifyFX on 1/12/2016.
 */

public class FoodOrder implements Parcelable {
    FoodItem food;
    int qty;

    public FoodOrder(FoodItem food, int qty) {
        this.food = food;
        this.qty = qty;
    }

    public FoodOrder() {

    }

    public FoodItem getFood() {
        return food;
    }

    public void setFood(FoodItem food) {
        this.food = food;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeValue(food);
        out.writeInt(qty);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<FoodOrder> CREATOR = new Parcelable.Creator<FoodOrder>() {
        public FoodOrder createFromParcel(Parcel in) {
            return new FoodOrder(in);
        }

        public FoodOrder[] newArray(int size) {
            return new FoodOrder[size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private FoodOrder(Parcel in) {
        food = (FoodItem) in.readValue(FoodItem.class.getClassLoader());
        qty = in.readInt();
    }
}
