package th.in.vector.elrestaurante;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by GGG on 1/12/2559.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public void onMessageReceived(RemoteMessage remotemesssage){
        for(String k:remotemesssage.getData().keySet()){
            String s = remotemesssage.getData().get(k);
            Log.i("Output","onMessagerecieved: "+s);
            System.out.println("onMessagerecieved: "+s);
        }

        //message comes in JSON format
        if(remotemesssage.getNotification()!=null){
            String s = remotemesssage.getNotification().getBody();
            String Title = remotemesssage.getNotification().getTitle();
            String Body = remotemesssage.getNotification().getBody();
            Log.i("Output","onMessageReceived: "+s);

            //on notification popup on the mobile phone actionbar
            NotificationCompat.Builder builder = new NotificationCompat.Builder(MyFirebaseMessagingService.this);
            builder.setSmallIcon(R.drawable.cast_ic_notification_small_icon);
            builder.setContentTitle(Title);
            builder.setContentText(Body);
            builder.setWhen(System.currentTimeMillis()+10);

            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0,builder.build());
        }
    }
}