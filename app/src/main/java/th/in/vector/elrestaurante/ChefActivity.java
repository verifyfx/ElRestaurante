package th.in.vector.elrestaurante;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ChefActivity extends AppCompatActivity implements SensorEventListener {

    private TextView startOver;
    private String table_code = "table_code";

    private SensorManager sensorManager;
    private long lastUpdate;
    private boolean isShaken = false;

    private float mAcc;
    private float mAccCurr;
    private float mAccLast;
    private DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chef);

        //THIS IS MAGIC///////////////////////////////////////////////////////
        Intent incomingIntent = getIntent();
        Bundle inBundle = incomingIntent.getExtras();
        if(inBundle!=null) {
            table_code = inBundle.getString(MainActivity.EXTRA_TABLE_CODE);
        }
        //////////////////////////////////////////////////////////////////////

        mDatabase = FirebaseDatabase.getInstance().getReference();

        //make connection
        sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        lastUpdate = System.currentTimeMillis();

        table_code = getIntent().getStringExtra(MainActivity.EXTRA_TABLE_CODE);

        startOver = (TextView)findViewById(R.id.startOver);
        startOver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //set db to inform that the table is free
                mDatabase.child("table").child(table_code).child("is_using").setValue(false);


                //get back to the first page
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                startActivity(i);
                finish();
            }
        });
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            //set db to inform that the table is free
            mDatabase.child("table").child(table_code).child("is_using").setValue(false);


            //get back to the first page
            Intent i = getBaseContext().getPackageManager()
                    .getLaunchIntentForPackage( getBaseContext().getPackageName() );
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            startActivity(i);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // register this class as a listener for the orientation and
        // accelerometer sensors
        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        // unregister listener
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType()== Sensor.TYPE_ACCELEROMETER){
            getAccelerometer(event);
        }
    }

    //detect Accelerometer
    private void getAccelerometer(SensorEvent event) {
        float[] values = event.values;

        float x = values[0];
        float y = values[1];
        float z = values[2];

        mAccLast = mAccCurr;
        mAccCurr = (float) Math.sqrt(x * x + y * y + z * z);
        float delta = mAccCurr - mAccLast;
        mAcc = mAcc * 0.9f + delta;

        long actualTime = System.currentTimeMillis();

        // Make this higher or lower according to how much
        // motion you want to detect
            if(mAcc>=20){
                /*if (isShaken) {
                    Toast.makeText(ChefActivity.this,"Please wait! the waiters are human too!",Toast.LENGTH_SHORT).show();
                    return;
                }
                isShaken = true;
                lastUpdate = actualTime;*/

                if(actualTime - lastUpdate < 3000){
                    return;
                }
                lastUpdate = actualTime;
                //calling waiter/waitress
                Toast.makeText(ChefActivity.this,"The requested is sent",Toast.LENGTH_SHORT).show();

                //timestamp
                Calendar c = Calendar.getInstance();
                SimpleDateFormat dformat = new SimpleDateFormat("dd-MMM-yyyy hh:mm aaa");
                String timestamp = dformat.format(c.getTime());

                //get key
                String newPost = mDatabase.child("waiter").push().getKey();
                ServiceRequest request = new ServiceRequest(FirebaseInstanceId.getInstance().getToken(),table_code,false,timestamp);
                mDatabase.child("waiter").child(newPost).setValue(request);

            }
    }
}

