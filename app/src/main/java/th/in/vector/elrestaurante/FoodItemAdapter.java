package th.in.vector.elrestaurante;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by VerifyFX on 30/11/2016.
 */

public class FoodItemAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private List<FoodItem> mDataSource;

    public FoodItemAdapter(Context context, List<FoodItem> items) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public  int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int pos) {
        return mDataSource.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        View rowView = mInflater.inflate(R.layout.menu_picture, parent, false);
        ImageView thumbnail   = (ImageView) rowView.findViewById(R.id.thumbnail);
        TextView lblFoodTitle = (TextView) rowView.findViewById(R.id.lblFoodTitle);
        TextView lblFoodPrice = (TextView) rowView.findViewById(R.id.lblFoodQty);

        FoodItem food = (FoodItem) getItem(pos);
        lblFoodTitle.setText(food.getFoodName());
        lblFoodPrice.setText("Price: " + String.valueOf(food.getPrice()) + " USD");
        Picasso.with(mContext).load(food.getPictureUrl()).fit().into(thumbnail);

        return rowView;
    }
}