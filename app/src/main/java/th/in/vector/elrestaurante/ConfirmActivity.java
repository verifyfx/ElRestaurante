package th.in.vector.elrestaurante;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import static th.in.vector.elrestaurante.MenuActivity.EXTRA_SEND_CONFIRM;

public class ConfirmActivity extends AppCompatActivity {

    private Intent intent;
    private ArrayList<FoodOrder> food = null;
    private FoodItemAdapter2 adapter;
    private ListView listView;
    private TextView btnSendChef;
    private String table_code = "";

    private FirebaseDatabase database;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);

        intent = getIntent();
        Bundle bundle = intent.getExtras();

        if(bundle!=null) {
            food = bundle.getParcelableArrayList(EXTRA_SEND_CONFIRM);
            table_code = bundle.getString(MainActivity.EXTRA_TABLE_CODE);
            Log.d("CONFIRM", "Loaded " + food.size());
        }

        listView = (ListView) findViewById(R.id.list_view);
        updateListView();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long viewId = view.getId();

                if (viewId == R.id.delBtn) {
                    //Toast.makeText(ConfirmActivity.this, "Button del clicked", Toast.LENGTH_SHORT).show();
                    String foodname = food.get(position).getFood().getFoodName();
                    confirmDelete(foodname, position);
                }
            }
        });

        btnSendChef = (TextView) findViewById(R.id.btnSendChef);
        btnSendChef.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("DD", String.valueOf(event.getAction()));
                if(event.getAction() == 0) {
                    Intent forwardIntent = new Intent(v.getContext() , ChefActivity.class);
                    forwardIntent.putExtra(MainActivity.EXTRA_TABLE_CODE,table_code);

                    database = FirebaseDatabase.getInstance();
                    DatabaseReference myRef = database.getReference().child("order");
                    String key = myRef.push().getKey();
                    DatabaseReference myFoodRef = myRef.child(key);
                    DatabaseReference mySingleFood = myFoodRef.child("food");
                    for(FoodOrder fo : food) {
                        String sKey = mySingleFood.push().getKey();
                        DatabaseReference mySingleFood2 = mySingleFood.child(sKey);
                        mySingleFood2.child("name").setValue(fo.getFood().getFoodName());
                        mySingleFood2.child("price").setValue(fo.getFood().getPrice());
                        mySingleFood2.child("qty").setValue(fo.getQty());
                    }
                    myFoodRef.child("table_code").setValue(table_code);

                    startActivity(forwardIntent);
                    return true;
                }
                return false;
            }
        });
    }

    private void updateListView() {
        listView = (ListView) findViewById(R.id.list_view);
        adapter = new FoodItemAdapter2(this, food);
        listView.setAdapter(adapter);
    }

    private void confirmDelete(String foodName, final int pos) {
        new AlertDialog.Builder(this)
                .setTitle("Are you sure?")
                .setMessage("Do you really want to remove "+foodName+"?")
                //.setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        food.remove(pos);
                        updateListView();
                        if(food.size()==0) finish();
                    }})
                .setNegativeButton(android.R.string.no, null).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                confirmBack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            confirmBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void confirmBack() {
        new AlertDialog.Builder(this)
                .setTitle("Are you sure?")
                .setMessage("Do you really want to go back? All your order will be lost!")
                //.setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //set db to inform that the table is free
                        mDatabase = FirebaseDatabase.getInstance().getReference();
                        mDatabase.child("table").child(table_code).child("is_using").setValue(false);


                        //get back to the first page
                        Intent i = getBaseContext().getPackageManager()
                                .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        startActivity(i);
                        finish();
                    }})
                .setNegativeButton(android.R.string.no, null).show();
    }
}
