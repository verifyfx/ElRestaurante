package th.in.vector.elrestaurante;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by VerifyFX on 30/11/2016.
 */

//public class FoodItem {
public class FoodItem implements Parcelable {
    private String foodName;
    private String desc;
    private Uri pictureUrl;
    private double price;

    public FoodItem(String foodName, String desc, Uri pictureUrl, double price) {
        this.foodName = foodName;
        this.desc = desc;
        this.pictureUrl = pictureUrl;
        this.price = price;
    }

    public FoodItem() {

    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Uri getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(Uri pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    public void setPrice(String price) {
        this.price = Double.parseDouble(price);
    }

    // 99.9% of the time you can just ignore this
    @Override
    public int describeContents() {
        return 0;
    }

    // write your object's data to the passed-in Parcel
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(foodName);
        out.writeString(desc);
        out.writeString(String.valueOf(pictureUrl));
        out.writeDouble(price);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<FoodItem> CREATOR = new Parcelable.Creator<FoodItem>() {
        public FoodItem createFromParcel(Parcel in) {
            return new FoodItem(in);
        }

        public FoodItem[] newArray(int size) {
            return new FoodItem[size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private FoodItem(Parcel in) {
        foodName = in.readString();
        desc = in.readString();
        pictureUrl = Uri.parse(in.readString());
        price = in.readDouble();
    }
}
