package th.in.vector.elrestaurante;

/**
 * Created by GGG on 1/12/2559.
 */

public class ServiceRequest {
    private String call_by;
    private String tableCode;
    private boolean is_served;
    private String timestamp;

    public ServiceRequest(String call_by, String tableCode, boolean status, String timestamp) {
        this.call_by = call_by;
        this.tableCode = tableCode;
        this.is_served = status;
        this.timestamp = timestamp;
    }

    public ServiceRequest() {

    }

    public String getToken() { return call_by; }

    public void setToken(String call_by) {
        this.call_by = call_by;
    }

    public String gettableCode() {
        return tableCode;
    }

    public void settableCode(String tableCode) {
        this.tableCode = tableCode;
    }

    public boolean setIsServed() { return is_served; }

    public void setIsServed(boolean status) {
        this.is_served = status;
    }

    public String getTimestamp() { return timestamp; }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
