package th.in.vector.elrestaurante;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public final static String EXTRA_TABLE_CODE = "EXTRA_TBL_CODE";
    TextView btnEnter;
    TextView txtTableCode;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    FirebaseDatabase database;
    boolean location_check = false;
    boolean code_check = false;
    private ProgressDialog mProgressDialog;
    private String tableNode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtTableCode = (TextView) findViewById(R.id.txtTableCode);

        btnEnter = (TextView) findViewById(R.id.btnEnter);
        btnEnter.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                performLogin();
                return false;
            }
        });

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        database = FirebaseDatabase.getInstance();

        mProgressDialog = new ProgressDialog(this);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    private boolean performLogin() {
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.show();
        //get current location
        if (mLastLocation != null) {
            double lat = mLastLocation.getLatitude();
            double lon = mLastLocation.getLongitude();
            float accu = mLastLocation.getAccuracy();
            Log.d("MAIN", " LAT:" + String.valueOf(lat));
            Log.d("MAIN", " LON:" + String.valueOf(lon));
            Log.d("MAIN", "ACCU:" + String.valueOf(accu));
            float[] distance = new float[2];
            Location.distanceBetween( lat, lon, 13.7947609, 100.3211958, distance);

            Log.d("MAIN", "DIST:" + String.valueOf(distance[0]));
            if( distance[0] > accu+2000){
                //Toast.makeText(getBaseContext(), "Outside", Toast.LENGTH_LONG).show();
            } else {
                //Toast.makeText(getBaseContext(), "Inside", Toast.LENGTH_LONG).show();
                location_check = true;
            }
        }
        //location_check = true;

        //get table code
        final String tableCode = txtTableCode.getText().toString();
        DatabaseReference ref = database.getReference("table");
        Query query = ref.orderByChild("code").equalTo(tableCode).limitToFirst(1);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    for(DataSnapshot table : dataSnapshot.getChildren()) {
                        tableNode = table.getKey();
                        boolean is_using = table.child("is_using").getValue(Boolean.class);
                        if(!is_using) code_check = true;
                        Log.d("DB", String.valueOf(table.child("is_using").getValue(Boolean.class)));
                        if(is_using) Log.d("DB", "The table is currently in use");
                    }
                } else {
                    Log.d("DB", String.valueOf("NO SUCH TABLE"));
                }

                mProgressDialog.dismiss();
                if (location_check && code_check) {
                    DatabaseReference reff = database.getReference("table").child(tableNode).child("is_using");
                    reff.setValue(true);
                    Toast.makeText(MainActivity.this, "Welcome!",
                            Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, MenuActivity.class);
                    intent.putExtra(EXTRA_TABLE_CODE, tableNode);
                    startActivity(intent);
                } else {
                    if(!location_check) {
                        Toast.makeText(MainActivity.this, "Wrong Location",
                                Toast.LENGTH_SHORT).show();
                    }
                    if(!code_check) {
                        Toast.makeText(MainActivity.this, "Incorrect Code",
                                Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return false;
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_COARSE_LOCATION}, 666);
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//        mLastLocation.getLatitude();
//        mLastLocation.getLongitude();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

    }

    @Override
    public void onConnectionSuspended(int cause) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {

    }
}
