package th.in.vector.elrestaurante;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MenuActivity extends AppCompatActivity {

    public final static String EXTRA_FOOD_ID = "MENU_EXTRA_-1";
    public final static String EXTRA_FOOD_NAME = "MENU_EXTRA_0";
    public final static String EXTRA_DESC = "MENU_EXTRA_1";
    public final static String EXTRA_PICTURE = "MENU_EXTRA_2";
    public final static String EXTRA_PRICE = "MENU_EXTRA_3";
    public final static String EXTRA_RETURN_QTY = "MENU_EXTRA_4";
    public final static String EXTRA_SEND_CONFIRM = "MENU_EXTRA_5";
    public final static int REQUEST_QTY = 666;
    private String table_code = "";

    private Toolbar toolbar;
    private ListView listView;
    private FoodItemAdapter adapter;
    private ProgressDialog mProgressDialog;
    private TextView btnOrder;

    private List<FoodItem> mDataSource = new ArrayList<>();
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRef = database.getReference("menu");
    private boolean menu_loaded = false;

    private ArrayList<FoodOrder> carts = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent incomingIntent = getIntent();
        Bundle inBundle = incomingIntent.getExtras();
        if(inBundle!=null) {
            table_code = inBundle.getString(MainActivity.EXTRA_TABLE_CODE);
        }

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Loading Menu...");
        mProgressDialog.show();

        btnOrder = (TextView)findViewById(R.id.btnSendChef);

        listView = (ListView) findViewById(R.id.list_view);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d("MENU", "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d("MENU", "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };
        mAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d("MENU", "signInAnonymously:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w("MENU", "signInAnonymously", task.getException());
                            Toast.makeText(MenuActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                if(!menu_loaded) {
                    if(dataSnapshot != null) {
                        Iterable<DataSnapshot> data = dataSnapshot.getChildren();
                        for(DataSnapshot item: data) {
                            String tname = item.child("name").getValue(String.class);
                            String tdesc = item.child("desc").getValue(String.class);
                            Uri tUri  = Uri.parse(item.child("picture").getValue(String.class));
                            Double tprice = Double.parseDouble(item.child("price").getValue(String.class));

                            mDataSource.add(new FoodItem(tname, tdesc, tUri, tprice));
                            Log.d("FirebaseDB/MenuAct", "Menu: " + tname+ " loaded with ["+tdesc+", "+tUri+", "+String.valueOf(tprice));
                        }
                        updateListView();
                        mProgressDialog.dismiss();
                        menu_loaded = true;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("FirebaseDB", "Failed to read value.", error.toException());
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, final View view, int i, long l) {
                final FoodItem food = (FoodItem) listView.getAdapter().getItem(i);

                Intent intent = new Intent(view.getContext() , FoodDetailActivity.class);
                intent.putExtra(EXTRA_FOOD_ID, i);
                intent.putExtra(EXTRA_FOOD_NAME, food.getFoodName());
                intent.putExtra(EXTRA_DESC, food.getDesc());
                intent.putExtra(EXTRA_PICTURE, String.valueOf(food.getPictureUrl()));
                intent.putExtra(EXTRA_PRICE, food.getPrice());
                ((Activity) view.getContext()).startActivityForResult(intent, REQUEST_QTY);
            }
        });

        btnOrder = (TextView) findViewById(R.id.btnSendChef);
        btnOrder.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("DD", String.valueOf(event.getAction()));
                if(event.getAction() == 0) {
                    if(carts.size() == 0) {
                        Toast.makeText(MenuActivity.this, "Please order something first.", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                    Intent forwardIntent = new Intent(v.getContext() , ConfirmActivity.class);
                    forwardIntent.putExtra(MainActivity.EXTRA_TABLE_CODE,table_code);
                    forwardIntent.putParcelableArrayListExtra(EXTRA_SEND_CONFIRM, carts);
                    startActivity(forwardIntent);
                    carts.clear();
                    updateListView();
                    Log.d("YOYOYOYOYOOYOYOYOYO","YO");
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mAuth.signOut();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private void updateListView() {
        listView = (ListView) findViewById(R.id.list_view);
        adapter = new FoodItemAdapter(this, mDataSource);
        listView.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intentRet) {
        int foodId=-1;
        int qty=-1;
        if (requestCode == REQUEST_QTY) {
            if(resultCode == Activity.RESULT_OK) {
                foodId = intentRet.getIntExtra(EXTRA_FOOD_ID, -1);
                qty = intentRet.getIntExtra(EXTRA_RETURN_QTY,-1);
                Log.d("Order", "ID: "+ String.valueOf(foodId) +" VAL: "+String.valueOf(qty));
                FoodItem food = (FoodItem) listView.getAdapter().getItem(foodId);
                carts.add(new FoodOrder(food, qty));
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }
}
